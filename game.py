#!/usr/bin/python
# -*- coding: utf8 -*-

############################################################################
#	 PPPoker - Play Python Poker										   #
#    game.py: Important game functions                                     #
############################################################################
#    Copyright (C) 2009 Lisa Vitolo 								       #
#																		   #
#    This program is free software: you can redistribute it and/or modify  #
#    it under the terms of the GNU General Public License as published by  #
#    the Free Software Foundation, either version 3 of the License, or	   #
#    (at your option) any later version.								   #
#																		   #
#    This program is distributed in the hope that it will be useful,	   #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of		   #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the		   #
#    GNU General Public License for more details.						   #
#																		   #
#    You should have received a copy of the GNU General Public License     #
#    along with this program.  If not, see <http://www.gnu.org/licenses/>. #
############################################################################

import random
from player import Player
from utility import *

def ChangeComputerCards(hand, deck):
	temp = Player()
	temp.SetHand(hand)
	temp.SolveHand()
	
	res = temp.GetResult()
	plNums = temp.GetNumsFromHand()
	
	newHand = []
	
	if res == 1: # Two of a kind
		change = 3
		type = temp.GetType()
		
		for i in range(5):
			if plNums[i] == type:
				newHand.append(hand[i])
	
	elif res == 2: # Two pairs
		change = 1
		type = temp.GetType()
		double_type = temp.GetDoubleType()
		
		for i in range(5):
			if plNums[i] == type or plNums[i] == double_type:
				newHand.append(hand[i])
	
	elif res == 3: # Three of a kind
		change = 2
		type = temp.GetType()
		
		for i in range(5):
			if plNums[i] == type:
				newHand.append(hand[i])

	elif res == 0: # No score
		change = 4 # Change 4 random cards
		
		random.seed()
		index = random.randint(0, 4)
		newHand.append(hand[index])

	else:
		return hand
		
	newHand += GetCards(change, deck)
	return newHand

def DecidePcAction(desk):
	prevBet = desk.myMoney.GetBet()
	budget = desk.compMoney.GetBudget()
	
	# If it doesn't have enough money, it's forced to exit game
	if budget < prevBet:
		return False
		
	else:
		# Choice random action to follow
		random.seed()
		action = random.randint(0, 1)
		
		# Computer closes game
		if action == 1 or prevBet == budget:
			desk.compMoney.AdjustBudget(prevBet, False)
			desk.AddDesk(prevBet)
			return True
		
		# Computer makes another offer	
		elif action == 0:
			if budget < 300:
				newOffer = random.randint(prevBet + 1, budget)
			else:
				newOffer = random.randint(prevBet + 1, 300)
			desk.compMoney.SetBet(newOffer)
			desk.AddDesk(newOffer)
			desk.compMoney.AdjustBudget(newOffer, False)
			return newOffer


def CheckWinner(pl1, pl2):
	# Who has the most important combination?
	if pl1.GetResult() > pl2.GetResult():
		return 1
	elif pl1.GetResult() < pl2.GetResult():
		return 2
		
	# Okay...who has the most important cards in his combination?	
	else:
		type1 = pl1.GetType()
		type2 = pl2.GetType()
		if type1 != 0 and type1 > type2:
			return 1
		elif type1 != 0 and type1 < type2:
			return 2
			
		# Last hope
		else:
			return CompareHighest(pl1, pl2)
			
def CompareHighest(pl1, pl2):
	pNums1 = pl1.GetNumsFromHand()
	pNums2 = pl2.GetNumsFromHand()
	
	pNums1.sort()
	pNums2.sort()
	
	for i in range(4, 0, -1):
		if pNums1[i] > pNums2[i]:
			return 1
		elif pNums1[i] < pNums2[i]:
			return 2
	
	return 0
