#!/usr/bin/python
# -*- coding: utf8 -*-

############################################################################
#	 PPPoker - Play Python Poker										   #
#    mainWindow.py: Pure graphic part                                      #
############################################################################
#    Copyright (C) 2009 Lisa Vitolo 								       #
#																		   #
#    This program is free software: you can redistribute it and/or modify  #
#    it under the terms of the GNU General Public License as published by  #
#    the Free Software Foundation, either version 3 of the License, or	   #
#    (at your option) any later version.								   #
#																		   #
#    This program is distributed in the hope that it will be useful,	   #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of		   #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the		   #
#    GNU General Public License for more details.						   #
#																		   #
#    You should have received a copy of the GNU General Public License     #
#    along with this program.  If not, see <http://www.gnu.org/licenses/>. #
############################################################################

from desk import Desk
from PyQt4 import QtCore, QtGui

class MainWindow(QtGui.QMainWindow):
	def __init__(self, desk):
		QtGui.QMainWindow.__init__(self)
	
		self.setWindowTitle('PPPoker v0.1')
		self.curDesk = desk

		self.myMoney = self.curDesk.myMoney
		self.compMoney = self.curDesk.compMoney

		self.createCentralWidget()
		self.createMenus()

	def createCentralWidget(self):
		self.CheckBoxes = []
		self.widget = QtGui.QWidget(self)
		
		self.grid = QtGui.QGridLayout(self.widget)
		self.grid.setSpacing(10)
		        
		cardLabel = QtGui.QLabel("Here are your cards.\nSelect those you want to change,\nthen click on the button:", self.widget)
		self.grid.addWidget(cardLabel, 0, 0)
		
		changeButton = QtGui.QPushButton('Change cards', self.widget)
		self.connect(changeButton, QtCore.SIGNAL('clicked()'), self.ChangeCards)
		self.grid.addWidget(changeButton, 0, 2)

		cardVbox = QtGui.QVBoxLayout()
		cardVbox.setSpacing(2)

		# These five checkboxes will display the user's cards
		# See Game.PrintCardCheckBoxes
		for x in range(5):
			checkBox = QtGui.QCheckBox("", self.widget)
			cardVbox.addWidget(checkBox)
			self.CheckBoxes.append(checkBox)
		self.grid.addLayout(cardVbox, 0, 1)

		budgetLabel = QtGui.QLabel("You = " + str(self.myMoney.GetBudget()) + "$\nComputer = " + str(self.compMoney.GetBudget()) + "$", self.widget)
		stupidLabel = QtGui.QLabel("How much do you want to bet?", self.widget)
		deskLabel = QtGui.QLabel("Desk = " + str(self.curDesk.GetDesk()), self.widget)
		self.dLabels = [budgetLabel, stupidLabel, deskLabel]
		
		self.grid.addWidget(budgetLabel, 1, 0)
		self.grid.addWidget(stupidLabel, 2, 0)
		self.grid.addWidget(deskLabel, 3, 0)
		
		self.dTextEdit = QtGui.QTextEdit(self.widget)
		self.dTextEdit.setReadOnly(True)
		self.grid.addWidget(self.dTextEdit, 1, 1, 3, 2)
		
		betButton = QtGui.QPushButton("Bet", self.widget)
		closeButton = QtGui.QPushButton("Close", self.widget)
		exitButton = QtGui.QPushButton("Exit", self.widget)
		self.connect(betButton, QtCore.SIGNAL('clicked()'), self.MoneyOffer)
		self.connect(closeButton, QtCore.SIGNAL('clicked()'), self.CloseMatch)
		self.connect(exitButton, QtCore.SIGNAL('clicked()'), self.ExitMatch)
		
		self.grid.addWidget(betButton, 4, 0)
		self.grid.addWidget(closeButton, 4, 1)
		self.grid.addWidget(exitButton, 4, 2)
		
		self.sector1 = [cardLabel, changeButton]
		self.sector2 = [budgetLabel, stupidLabel, deskLabel, self.dTextEdit, betButton, closeButton, exitButton]
		
		self.widget.setLayout(self.grid)
		self.setCentralWidget(self.widget)
        
	def createMenus(self):
		quit = self.createMenuVoice('icons/quit.png', 'Quit', 'Ctrl+Q', 'Quit application', QtCore.SLOT('close()'))
		pref = self.createMenuVoice('icons/info.png', 'Preferences', 'Ctrl+P', 'Open preferences', self.openPrefDialog)
		info = self.createMenuVoice('icons/info.png', 'Information', 'Ctrl+I', 'Show information about PPPoker', self.openInfoDialog)
        
		self.statusBar().show()
        
		menubar = self.menuBar()
		file = menubar.addMenu('&File')
		mod = menubar.addMenu('&Edit')
		about = menubar.addMenu('&About')
		file.addAction(quit)    
		mod.addAction(pref)
		about.addAction(info)
    
	def createMenuVoice(self, iconPath, name, shortcut, tip, slot):
		voice = QtGui.QAction(QtGui.QIcon(iconPath), name, self)
		voice.setShortcut(shortcut)
		voice.setStatusTip(tip)
		self.connect(voice, QtCore.SIGNAL('triggered()'), slot)
		
		return voice
	
	def openPrefDialog(self):
		items = QtCore.QStringList()
		items << self.tr("Italian") << self.tr("English")
    
		item, ok = QtGui.QInputDialog.getItem(self, self.tr("Select language"), self.tr("Select language (ATTENTION: you need to close\n PPPoker in order to apply changes):"), items, 0, False)
		if ok and not item.isEmpty():
			print "Add language choice"

	def openInfoDialog(self):
		QtGui.QMessageBox.information(self, self.tr("PPPoker v0.1"), "PPPoker v0.1\nDeveloped by Lisa \"shainer\" Vitolo, under GPL license\nAny feedback: syn.shainer@gmail.com")

	def reset(self):
		self.ShowCardWindow()
		self.HideDeskWindow()
		
