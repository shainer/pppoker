#!/usr/bin/python
# -*- coding: utf8 -*-

############################################################################
#	 PPPoker - Play Python Poker										   #
#    desk.py: Manage desk and bets  		                               #
############################################################################
#    Copyright (C) 2009 Lisa Vitolo 								       #
#																		   #
#    This program is free software: you can redistribute it and/or modify  #
#    it under the terms of the GNU General Public License as published by  #
#    the Free Software Foundation, either version 3 of the License, or	   #
#    (at your option) any later version.								   #
#																		   #
#    This program is distributed in the hope that it will be useful,	   #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of		   #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the		   #
#    GNU General Public License for more details.						   #
#																		   #
#    You should have received a copy of the GNU General Public License     #
#    along with this program.  If not, see <http://www.gnu.org/licenses/>. #
############################################################################

from money import Money

class Desk:
	desk = 0
	myMoney = Money()
	compMoney = Money()
	
	def AddDesk(self, value):
		self.desk += value

	def GetDesk(self):
		return self.desk

	def ZeroDesk(self):
		self.desk = 0
