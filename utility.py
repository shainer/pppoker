#!/usr/bin/python
# -*- coding: utf8 -*-

############################################################################
#	 PPPoker - Play Python Poker										   #
#    utility.py: Some general functions                                    #
############################################################################
#    Copyright (C) 2009 Lisa Vitolo 								       #
#																		   #
#    This program is free software: you can redistribute it and/or modify  #
#    it under the terms of the GNU General Public License as published by  #
#    the Free Software Foundation, either version 3 of the License, or	   #
#    (at your option) any later version.								   #
#																		   #
#    This program is distributed in the hope that it will be useful,	   #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of		   #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the		   #
#    GNU General Public License for more details.						   #
#																		   #
#    You should have received a copy of the GNU General Public License     #
#    along with this program.  If not, see <http://www.gnu.org/licenses/>. #
############################################################################

import random

def GetCards(num, deck):
	n = len(deck) - 1
	hand = []

	random.seed()

	# Takes num cards from the deck
	for i in range(num):
		j = random.randint(0, n)

		hand.append(deck[j])
		deck.pop(j)

		n -= 1
	
	return hand
	
def GetNewDeck():
	deck = ['8H', '9H', 'TH', 'JH', 'QH', 'KH', 'AH', '8D', '9D', 'TD', 'JD', 'QD', 'KD', 'AD', '8C', '9C', 'TC', 'JC', 'QC', 'KC', 'AC', '8S', '9S', 'TS', 'JS', 'QS', 'KS', 'AS']
	return deck

def PrintHand(h):
	cards = []
	sCard = ""
	
	for card in h:
		if card[0] == 'T':
			sCard = "10"
		elif card[0] == 'J':
			sCard = "Jack"
		elif card[0] == 'Q':
			sCard = "Queen"
		elif card[0] == 'K':
			sCard = "King"
		elif card[0] == 'A':
			sCard = "Ace"
		else:
			sCard = str(card[0])
		
		sCard += " of "
		if card[1] == 'H':
			sCard += "Hearts"
		elif card[1] == 'D':
			sCard += "Diamonds"
		elif card[1] == 'C':
			sCard += "Clubs"
		elif card[1] == 'S':
			sCard += "Spades"
		cards.append(sCard)
		
	return cards
