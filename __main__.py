#!/usr/bin/python
# -*- coding: utf8 -*-

############################################################################
#	 PPPoker - Play Python Poker										   #
#    __main__.py: Main files and classes                                   #
############################################################################
#    Copyright (C) 2009 Lisa Vitolo 								       #
#																		   #
#    This program is free software: you can redistribute it and/or modify  #
#    it under the terms of the GNU General Public License as published by  #
#    the Free Software Foundation, either version 3 of the License, or	   #
#    (at your option) any later version.								   #
#																		   #
#    This program is distributed in the hope that it will be useful,	   #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of		   #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the		   #
#    GNU General Public License for more details.						   #
#																		   #
#    You should have received a copy of the GNU General Public License     #
#    along with this program.  If not, see <http://www.gnu.org/licenses/>. #
############################################################################

import sys
import random
from PyQt4 import QtGui, QtCore

from player import Player
from desk import Desk
from mainWindow import MainWindow

from utility import *
from game import *

class Game(MainWindow):
	
	def __init__(self, desk, firstTime):
		
		# Init necessary objects
		self.fstPlayer = Player()
		self.secPlayer = Player()
		self.deck = GetNewDeck()

		# Init current hand for both player
		self.fstHand = GetCards(5, self.deck)
		self.fstPlayer.SetHand(self.fstHand)
		
		# As you may notice, I'm not setting the computer hand.
		# Otherwise there will be problems when I have to change its cards.
		self.secHand = GetCards(5, self.deck)
		
		# If it's the first time the procedure is called,
		# init the window and all the needed widgets
		if firstTime == 0:
			MainWindow.__init__(self, desk)
		
		# There are two "parts" of the window: the Card Window, where the cards are displayed
		# and the Desk Window, which manages bets and the desk.
		self.HideDeskWindow()
		for x in self.CheckBoxes:
			x.setCheckState(0)
		
		self.myMoney.SetBet(0)
		self.compMoney.SetBet(0)
		
		self.dTextEdit.setText("")
		self.disable_button = False
		self.PrintCardsCheckBoxes()
	
	def PrintCardsCheckBoxes(self):
		cards = PrintHand(self.fstHand) # returns the cards' name in a more friendly way
		
		for x in range(5):
			self.CheckBoxes[x].setText(cards[x])

	def ChangeCards(self):
		if self.disable_button == True:
			return
		
		newHand = []
		counter = 0
		
		for x in range(5):
			if self.CheckBoxes[x].checkState() == 0:
				newHand.append(self.fstHand[x])
				counter += 1

		# The user tried to change all 5 cards
		if counter == 0:
			self.showErrorDialog("You can change up to 4 cards")
			return
			
		if counter != 5:
			newHand += GetCards(5 - counter, self.deck)
			self.fstHand = newHand
			self.fstPlayer.SetHand(self.fstHand)
			self.PrintCardsCheckBoxes()
		
		self.secHand = ChangeComputerCards(self.secHand, self.deck)
		self.secPlayer.SetHand(self.secHand)
		
		self.disable_button = True
		self.ShowDeskWindow()

	def MoneyOffer(self):
		min = self.compMoney.GetBet() + 1
		max = 300
		
		if self.myMoney.GetBudget() <= min:
			self.ExitMatch()
		
		if self.myMoney.GetBudget() < 300:
			max = self.myMoney.GetBudget()
		
		offer, ok = QtGui.QInputDialog.getInteger(self, "Make your offer", "How much do you offer? (max 300)", 100, min, max, 1)
		
		if ok:
			self.myMoney.SetBet(offer)
			self.dTextEdit.append("You bet " + str(offer) + "$")
			self.curDesk.AddDesk(offer)
			self.myMoney.AdjustBudget(offer, False)	
			self.updateLabels()
			
			if offer == 300:
				self.CloseMatch("Computer ")
				
			pcBet = DecidePcAction(self.curDesk)
			if pcBet == False:
				self.ExitMatch("Computer ")
			elif pcBet == True:
				self.CloseMatch("Computer ")
			else:
				self.dTextEdit.append("Computer bet " + str(self.compMoney.GetBet()) + "$")
				self.updateLabels()
			
			
	def CloseMatch(self, who = "User "):		
		win = self.curDesk.GetDesk()
		self.curDesk.ZeroDesk()
		
		self.dTextEdit.append(who + "closes game")
		self.updateLabels()
		
		self.fstPlayer.SolveHand()
		self.secPlayer.SolveHand()
		retvalue = CheckWinner(self.fstPlayer, self.secPlayer)

		if (retvalue == 1):
			self.myMoney.AdjustBudget(win, True)
		else:
			self.compMoney.AdjustBudget(win, True)
		
		self.showFinalDialog(retvalue)
		self.widget.destroy()
		self.widget.show()
	
	def ExitMatch(self, who = "User "):		
		self.dTextEdit.append(who + "exits game")
		self.showFinalDialog(0)

	# Displays final information
	def showFinalDialog(self, value):
		if value == 1:
			message = "*** YOU WON! ***\n\n"
		elif value == 2:
			message = "*** YOU LOST! ***\n\n"
		else:
			message = "There is no winner in this round.\n\n"
			
		message += "Computer cards were:\n"
		pcCards = PrintHand(self.secHand)
		
		for x in range(5):
			message += pcCards[x] + "\n"
		
		QtGui.QMessageBox.information(self, "Information", message)
		self.reset()
		
		if self.curDesk.myMoney.GetBudget() <= 0:
			QtGui.QMessageBox.information(self, "Loser!", "You definitely lost!")
			exit()
		elif self.curDesk.compMoney.GetBudget() <= 0:
			QtGui.QMessageBox.information(self, "Winner!", "You definitely won over the computer!")
			exit()
						
		Game.__init__(self, self.curDesk, 1)
		
	
	def updateLabels(self):
		self.dLabels[0].setText("You = " + str(self.curDesk.myMoney.GetBudget()) + "$\nComputer = " + str(self.curDesk.compMoney.GetBudget()) + "$")
		self.dLabels[2].setText("Desk = " + str(self.curDesk.GetDesk()) + "$")
		
	def showErrorDialog(self, text):
		QtGui.QMessageBox.critical(self, "Error", text, QtGui.QMessageBox.Ok)
		

	def ShowDeskWindow(self):
		for x in self.sector2:
			x.show()
			
	def HideDeskWindow(self):
		for x in self.sector2:
			x.hide()
			
	def ShowCardWindow(self):
		for x in self.sector1:
			x.show()


if __name__ == "__main__":
	app = QtGui.QApplication(sys.argv)
	
	newGame = Game(Desk(), 0)
	newGame.show()
	
	sys.exit(app.exec_())
